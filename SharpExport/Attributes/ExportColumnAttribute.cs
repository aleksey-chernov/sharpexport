﻿using System;

namespace SharpExport.Attributes
{
    public enum HorizontalAlignment
    {
        Left,
        Right,
        Center
    };

    public enum VerticalAlignment
    {
        Top,
        Bottom,
        Center
    };

    /// <summary>
    /// Use on class properties to control layout of exported table columns.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ExportColumnAttribute : ResourceAttribute
    {
        private readonly Type _resourceType;
        private string _name;

        public ExportColumnAttribute()
        {
            Index = int.MaxValue;
            WrapText = true;
        }

        public ExportColumnAttribute(Type resourceType) : this()
        {
            _resourceType = resourceType;
        }

        public string Name
        {
            get
            {
                if (_resourceType != null && NameResource != null)
                {
                    return GetResourceLookup(_resourceType, NameResource);
                }
                
                return _name;
            }
            set { _name = value; }
        }

        public string NameResource { get; set; }

        public int Index { get; set; }

        public double Width { get; set; }

        public bool WrapText { get; set; }

        public HorizontalAlignment HAlign { get; set; }

        public VerticalAlignment VAlign { get; set; }
    }
}