﻿using System;

namespace SharpExport.Attributes
{
    /// <summary>
    /// Use on class to control exported table layout.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ExportTableAttribute : ResourceAttribute
    {
        private readonly Type _resourceType;
        private string _name;

        public ExportTableAttribute() { }

        public ExportTableAttribute(Type resourceType) : this()
        {
            _resourceType = resourceType;
        }

        public string Name
        {
            get
            {
                if (_resourceType != null && NameResource != null)
                {
                    return GetResourceLookup(_resourceType, NameResource);
                }
                
                return _name;
            }
            set { _name = value; }
        }

        public string NameResource { get; set; }
    }
}