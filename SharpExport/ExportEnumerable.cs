﻿using System.Collections.Generic;
using System.Linq;
using ClosedXML.Excel;
using SharpExport.Attributes;

namespace SharpExport
{
    public static class ExportEnumerable
    {
        /// <summary>
        /// Returnes ClosedXML workbook with table constructed from provided objects. Use attributes to control table layout.
        /// </summary>
        /// <typeparam name="TSource">Source type.</typeparam>
        /// <param name="source">Source enumeration.</param>
        /// <param name="ignore">Array of property names, which won't be included on constructed table.</param>
        /// <returns></returns>
        public static XLWorkbook ToXLWorkbook<TSource>(this IEnumerable<TSource> source, string[] ignore = null)
        {
            var wb = new XLWorkbook();

            var tableAttribute = typeof (TSource).GetCustomAttributes(typeof (ExportTableAttribute), false)
                .OfType<ExportTableAttribute>().FirstOrDefault();

            var ws =
                wb.Worksheets.Add(tableAttribute != null && tableAttribute.Name != null
                    ? tableAttribute.Name
                    : typeof (TSource).Name);

            var props = ExportHelpers.GetMarkedProperties(typeof(TSource), ignore);
            
            foreach (var item in source.Select((value, i) => new {value, i}))
            {
                for (var i = 0; i < props.Count; i++)
                {
                    ws.Cell(2 + item.i, i + 1).Value = props[i].GetValue(item.value, null);
                }
            }

            for (var i = 0; i < props.Count; i++)
            {
                var colAttr = props[i].GetCustomAttributes(typeof(ExportColumnAttribute), false)
                    .OfType<ExportColumnAttribute>().First();

                ws.Cell(1, i + 1).Value = colAttr.Name ?? props[i].Name;

                if (colAttr.Width > 0d)
                {
                    ws.Column(i + 1).Width = ExportHelpers.PixelsToInches(colAttr.Width);
                }
                else
                {
                    ws.Column(i + 1).AdjustToContents();
                }

                var style = ws.Column(i + 1).Style;
                style.Alignment.WrapText = colAttr.WrapText;

                switch (colAttr.HAlign)
                {
                    case HorizontalAlignment.Right:
                        style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        break;
                    case HorizontalAlignment.Center:
                        style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        break;
                    default:
                        style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        break;
                }

                switch (colAttr.VAlign)
                {
                    case VerticalAlignment.Bottom:
                        style.Alignment.Vertical = XLAlignmentVerticalValues.Bottom;
                        break;
                    case VerticalAlignment.Center:
                        style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                        break;
                    default:
                        style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
                        break;
                }
            }
            
            var range = ws.Range(ws.FirstCellUsed().Address, ws.LastCellUsed().Address);
            range.CreateTable();

            return wb;
        }
    }
}