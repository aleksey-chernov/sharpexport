﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using SharpExport.Attributes;

namespace SharpExport
{
    internal static class ExportHelpers
    {
        internal static double PixelsToInches(double pixels)
        {
            return (pixels - 12 + 5)/7d + 1;
        }

        internal static IList<PropertyInfo> GetMarkedProperties(Type type, string[] ignore = null)
        {
            var props = type.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(
                    prop =>
                        prop.GetCustomAttributes(typeof (ExportColumnAttribute), false)
                            .OfType<ExportColumnAttribute>()
                            .FirstOrDefault() != null);

            if (ignore != null)
            {
                props = props.Where(prop => !ignore.Contains(prop.Name));
            }

            props = props.OrderBy(
                prop =>
                    prop.GetCustomAttributes(typeof (ExportColumnAttribute), false)
                        .OfType<ExportColumnAttribute>()
                        .First()
                        .Index);

            return props.ToList();
        }
    }
}